use lambda_runtime::{service_fn, LambdaEvent, Error as LambdaError};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use std::collections::HashMap;
use log;
use std::error::Error;

#[derive(Deserialize, Debug)]
struct Request {
    productname: String,
}

#[derive(Serialize)]
struct Response {
    price: f64,
}

async fn search_product_price(client: &Client, productname: String) -> Result<f64, Box<dyn Error + Send + Sync>> {
    log::debug!("Searching for product price: {}", productname);
    let table_name = "week6-table";
    let mut expr_attr_values = HashMap::new();
    expr_attr_values.insert(":productname_val".to_string(), AttributeValue::S(productname.clone()));

    let result = client.query()
        .table_name(table_name)
        .key_condition_expression("productname = :productname_val")
        .set_expression_attribute_values(Some(expr_attr_values))
        .send()
        .await?;

    let item = result.items().and_then(|items| items.first());

    match item {
        Some(item) => {
            let price_attr = item.get("price").and_then(|val| val.as_n().ok());
            match price_attr {
                Some(price) => {
                    log::debug!("Price found: {}", price);
                    Ok(price.parse::<f64>().unwrap_or_default())
                },
                None => {
                    log::warn!("No price found for the given product name");
                    Err("No price found for the given product name".into()) // Convert the message into a Box<dyn Error>
                },
            }
        },
        None => {
            log::warn!("No product found with the name: {}", productname);
            Err("No product found with the given name".into()) // Convert the message into a Box<dyn Error>
        },
    }
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    log::info!("Received event: {:?}", event);
    let request: Request = serde_json::from_value(event.payload)?;
    log::debug!("Parsed request: {:?}", request);

    let config = load_from_env().await;
    let client = Client::new(&config);

    let price = search_product_price(&client, request.productname).await.map_err(LambdaError::from)?;
    log::info!("Found price: {}", price);

    Ok(json!({ "price": price }))
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    env_logger::init(); // Initialize env_logger
    log::info!("Starting lambda function");
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    log::info!("Lambda function finished");
    Ok(())
}

