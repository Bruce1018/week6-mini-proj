# Week6-mini-proj

> author: Zilin Xu
> netID: zx112


## Requirements display

### Add logging to a Rust Lambda function

check the `main.rs`

```rust
log::debug!("Searching for product price: {}", productname);


log::info!("Received event: {:?}", event);
    let request: Request = serde_json::from_value(event.payload)?;
    log::debug!("Parsed request: {:?}", request);


#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    env_logger::init(); // Initialize env_logger
    log::info!("Starting lambda function");
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    log::info!("Lambda function finished");
    Ok(())
}
```

### Integrate AWS X-Ray tracing
![](pic/截屏2024-03-07%2017.06.49.png)

### Connect logs/traces to CloudWatch

![](pic/截屏2024-03-07%2017.10.24.png)

![](pic/截屏2024-03-07%2017.09.27.png)

![](pic/截屏2024-03-07%2017.07.56.png)


## Steps 

0. Initialize the cargo lambda project
1. Use last week's `price menu` system into this week 
2. Add dependencies and code for `logging function`
3. Deploy Lambda function, create database and api as last week
4. Remeber change the `configuration` in Lambda as shown above
5. Test the api in `Postman` and see the logs/traces on `CloudWatch`